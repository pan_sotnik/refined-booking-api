This Rest Api creates a bus schedule which allows you to:
* add new buses
* add drivers and lines
* create tickets for specific buses
* sell tickets or change the bus scheduled to one of the others in the database.