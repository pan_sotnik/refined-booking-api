package com.booking.service_implementations;


import com.booking.domains.Bus;
import com.booking.domains.Line;
import com.booking.domains.Ticket;
import com.booking.instruments.ArgumentChecker;
import com.booking.repositories.LineRepository;
import com.booking.services.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LineServiceImpl implements LineService {

    private LineRepository lineRepository;

    @Autowired
    public void setLineRepository(LineRepository lineRepository){this.lineRepository = lineRepository;}


    @Override
    public void createLine(int lineNumber, Bus bus) {

            ArgumentChecker.checkForNull(bus);

            //initialize empty tickets set as specified in class constructor
            Set<Ticket> tickets = new HashSet<Ticket>();
            Line createdLine = new Line(lineNumber, bus, tickets);
            lineRepository.save(createdLine);

    }

    @Override
    public void updateLine(Line line, Bus newBus) {

            ArgumentChecker.checkForNull(line);
            ArgumentChecker.checkForNull(newBus);

            line.setBus(newBus);
            lineRepository.save(line);
    }

    @Override
    public Line findByNumber(int number) {
        return lineRepository.findByLineNumber(number);
    }

    @Override
    public void addTicketToLine(Line line) {
        lineRepository.save(line);
    }
}
