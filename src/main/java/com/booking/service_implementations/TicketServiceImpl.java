package com.booking.service_implementations;

import com.booking.domains.Line;
import com.booking.domains.Ticket;
import com.booking.instruments.ArgumentChecker;
import com.booking.instruments.IndividualTicketDTO;
import com.booking.instruments.IndividualTicketDTOCreator;
import com.booking.repositories.TicketRepository;
import com.booking.services.LineService;
import com.booking.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TicketServiceImpl implements TicketService {

    private LineService lineService;

    private TicketRepository ticketRepository;

    @Autowired
    public void setLineService(LineService lineService){this.lineService = lineService;}
    @Autowired
    public void setTicketRepository(TicketRepository ticketRepository){this.ticketRepository = ticketRepository;}

    @Override
    public void createTicket(int seat, int price, Line toAddTicketTo) {

            ArgumentChecker.checkForNull(toAddTicketTo);

            Ticket createdTicket = new Ticket(seat, price, false);

            Set<Ticket> tickets = toAddTicketTo.getTickets();

            for (Ticket ticket : tickets){
                if (ticket.getSeat() == seat){
                    throw new IllegalArgumentException();
                }
            }

            tickets.add(createdTicket);
            ticketRepository.save(createdTicket);

            lineService.addTicketToLine(toAddTicketTo);
    }

    @Override
    public Ticket findBySeat(int seat) {
        return ticketRepository.findBySeat(seat);
    }

    @Override
    public IndividualTicketDTO sellTicket(Line lineForTrip, Ticket forSale) {

        if (lineForTrip != null && !forSale.isPaid()){

            forSale.setPaid(true);
            ticketRepository.save(forSale);

            return IndividualTicketDTOCreator.generateIndividualTicketDTO(lineForTrip, forSale);

        }
        //This message will appear if such a ticket is not for sale
        throw new IllegalArgumentException("No such line scheduled or ticket already sold.");
    }
}
