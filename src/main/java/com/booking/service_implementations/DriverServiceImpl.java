package com.booking.service_implementations;

import com.booking.domains.Driver;
import com.booking.repositories.DriverRepository;
import com.booking.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DriverServiceImpl implements DriverService {

    private DriverRepository driverRepository;

    @Autowired
    public void setDriverRepository(DriverRepository driverRepository){this.driverRepository = driverRepository;}

    @Override
    public void createDriver(String firstName, String lastName, int license) {

        Driver driver = new Driver(firstName,lastName,license);

        driverRepository.save(driver);

    }

    @Override
    public Driver findByLastName(String lastName) {
        return driverRepository.findByLastName(lastName);
    }
}
