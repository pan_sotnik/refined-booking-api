package com.booking.service_implementations;

import com.booking.domains.Bus;
import com.booking.domains.Driver;
import com.booking.instruments.ArgumentChecker;
import com.booking.repositories.BusRepository;
import com.booking.services.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusServiceImpl implements BusService {

    private BusRepository busRepository;

    @Autowired
    public void setBusRepository(BusRepository busRepository){this.busRepository = busRepository;}

    @Override
    public void createBus(String model, int number, Driver driver) {

        ArgumentChecker.checkForNull(driver);

        Bus bus = new Bus(model, number, driver);

        busRepository.save(bus);

    }

    @Override
    public Bus findBusByNumber(int number) {
        return busRepository.findBusByNumber(number);
    }
}
