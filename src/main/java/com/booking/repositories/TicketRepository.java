package com.booking.repositories;

import com.booking.domains.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TicketRepository extends CrudRepository<Ticket, Integer> {

    Ticket findBySeat(@Param("seat")int seat);

}
