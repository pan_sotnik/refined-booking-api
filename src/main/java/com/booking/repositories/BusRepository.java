package com.booking.repositories;


import com.booking.domains.Bus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface BusRepository extends CrudRepository<Bus, Integer> {

    Bus findBusByNumber(@Param("number")int number);

}
