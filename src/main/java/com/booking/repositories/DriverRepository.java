package com.booking.repositories;

import com.booking.domains.Driver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DriverRepository extends CrudRepository<Driver, Integer> {

    Driver findByLastName(@Param("lastname") String lastName);

}
