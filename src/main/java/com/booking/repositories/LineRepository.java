package com.booking.repositories;


import com.booking.domains.Line;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface LineRepository extends CrudRepository<Line, Integer> {

    Line findByLineNumber(@Param("linenumber") int lineNumber);

}
