package com.booking.services;


import com.booking.domains.Bus;
import com.booking.domains.Driver;
import org.springframework.http.HttpStatus;

public interface BusService {

void createBus(String model, int number, Driver driver);

Bus findBusByNumber(int number);

}
