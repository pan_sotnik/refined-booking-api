package com.booking.services;


import com.booking.domains.Bus;
import com.booking.domains.Line;
import org.springframework.http.HttpStatus;

public interface LineService {

void createLine(int lineNumber, Bus bus);

void updateLine(Line line, Bus newBus);

Line findByNumber(int number);

void addTicketToLine(Line line);


}
