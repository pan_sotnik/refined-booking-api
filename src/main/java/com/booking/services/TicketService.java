package com.booking.services;


import com.booking.domains.Line;
import com.booking.domains.Ticket;
import com.booking.instruments.IndividualTicketDTO;
import org.springframework.http.HttpStatus;

public interface TicketService {

void createTicket(int seat, int price, Line toAddTicketTo);

Ticket findBySeat(int seat);

IndividualTicketDTO sellTicket(Line lineForTrip, Ticket forSale);

}
