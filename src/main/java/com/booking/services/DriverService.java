package com.booking.services;

import com.booking.domains.Driver;
import org.springframework.http.HttpStatus;


public interface DriverService {

    void createDriver(String firstName, String lastName, int license);

    Driver findByLastName(String lastName);

}
