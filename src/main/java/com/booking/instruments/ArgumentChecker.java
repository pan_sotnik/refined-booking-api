package com.booking.instruments;

public class ArgumentChecker {
//This class contains a reusable method for our many null checks
public static void checkForNull(Object argument){

    if (argument == null){
        throw new IllegalArgumentException();
    }

}

}
