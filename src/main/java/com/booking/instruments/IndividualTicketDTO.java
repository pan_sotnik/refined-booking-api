package com.booking.instruments;


public class IndividualTicketDTO {

    int lineNumber;
    int busNumber;
    String busModel;
    String driverFirstName;
    String driverLastName;
    int ticketPrice;
    int seat;

    public IndividualTicketDTO(int lineNumber, int busNumber, String busModel, String driverFirstName, String driverLastName, int ticketPrice, int seat) {
        this.lineNumber = lineNumber;
        this.busNumber = busNumber;
        this.busModel = busModel;
        this.driverFirstName = driverFirstName;
        this.driverLastName = driverLastName;
        this.ticketPrice = ticketPrice;
        this.seat = seat;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(int busNumber) {
        this.busNumber = busNumber;
    }

    public String getBusModel() {
        return busModel;
    }

    public void setBusModel(String busModel) {
        this.busModel = busModel;
    }

    public String getDriverFirstName() {
        return driverFirstName;
    }

    public void setDriverFirstName(String driverFirstName) {
        this.driverFirstName = driverFirstName;
    }

    public String getDriverLastName() {
        return driverLastName;
    }

    public void setDriverLastName(String driverLastName) {
        this.driverLastName = driverLastName;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

}
