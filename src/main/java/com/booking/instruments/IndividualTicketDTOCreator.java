package com.booking.instruments;

import com.booking.domains.Line;
import com.booking.domains.Ticket;

//This is a DTO factory
public class IndividualTicketDTOCreator {

public static IndividualTicketDTO generateIndividualTicketDTO(Line lineForTrip, Ticket forSale){

    int lineNumber = lineForTrip.getLineNumber();
    int busNumber = lineForTrip.getBus().getNumber();
    String busModel = lineForTrip.getBus().getModel();
    String driverFirstName = lineForTrip.getBus().getDriver().getName();
    String driverLastName = lineForTrip.getBus().getDriver().getLastName();
    int ticketPrice = forSale.getPrice();
    int seat = forSale.getSeat();

    return new IndividualTicketDTO(lineNumber, busNumber, busModel, driverFirstName, driverLastName, ticketPrice, seat);

}

}
