package com.booking.controllers;

import com.booking.domains.Line;
import com.booking.domains.Ticket;
import com.booking.instruments.IndividualTicketDTO;
import com.booking.services.LineService;
import com.booking.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketController {

private TicketService ticketService;

private LineService lineService;

    @Autowired
    public void setTicketService(TicketService ticketService){this.ticketService = ticketService;}
    @Autowired
    public void setLineService(LineService lineService){this.lineService = lineService;}

    @RequestMapping(value = "/tickets", method = RequestMethod.POST)
    //adds a ticket and assigns it to an existing line
    public HttpStatus createTicket(@RequestParam(value = "seat")int seat,
                                @RequestParam(value = "price")int price,
                                @RequestParam(value = "forline")int lineNumber) {

        Line toAddTicketTo = lineService.findByNumber(lineNumber);

        try {
            ticketService.createTicket(seat, price, toAddTicketTo);
        } catch (IllegalArgumentException i){
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.OK;

    }


    @RequestMapping(value = "/tickets", method = RequestMethod.GET)
    //Return a DTO so only relevant information is received
    public IndividualTicketDTO sellTicket(@RequestParam (value = "seat")int seat,
                                              @RequestParam (value = "line")int lineNumber) {


        Line lineForTrip = lineService.findByNumber(lineNumber);
        Ticket forSale = ticketService.findBySeat(seat);

        return ticketService.sellTicket(lineForTrip, forSale);


    }
}
