package com.booking.controllers;

import com.booking.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DriverController {

private DriverService driverService;

@Autowired
public void setDriverService(DriverService driverService){this.driverService = driverService;}

    @RequestMapping(value = "/drivers", method = RequestMethod.POST)
    //adds a driver to the database, this is the first action
    public HttpStatus createDriver(@RequestParam(value = "firstname")String firstName,
                                @RequestParam(value = "lastname")String lastName,
                                @RequestParam(value = "license")int license){

        driverService.createDriver(firstName, lastName, license);

        return HttpStatus.OK;
    }
}
