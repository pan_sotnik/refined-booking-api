package com.booking.controllers;

import com.booking.domains.Bus;
import com.booking.domains.Line;
import com.booking.services.BusService;
import com.booking.services.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LineController {

    private BusService busService;

    private LineService lineService;

    @Autowired
    public void setBusService(BusService busService){this.busService = busService;}
    @Autowired
    public void setLineService(LineService lineService){this.lineService = lineService;}

    @RequestMapping(value = "/lines", method = RequestMethod.POST)
    //adds a line and assigns a bus based on number
    public HttpStatus createLine(@RequestParam(value = "linenumber") int lineNumber,
                              @RequestParam(value = "busnumber") int busNumber) {

        Bus forService = busService.findBusByNumber(busNumber);

        try {
            lineService.createLine(lineNumber, forService);
        } catch (IllegalArgumentException i){
            return HttpStatus.BAD_REQUEST;
        }

        return HttpStatus.OK;

    }

    @RequestMapping(value = "/lines", method = RequestMethod.PUT)
    //takes line and bus arguments to select new bus for specified line
    public HttpStatus changeBus(@RequestParam(value = "linenumber") int lineNumber,
                                @RequestParam(value = "newbusnumber") int busNumber) {

        Line toChange = lineService.findByNumber(lineNumber);
        Bus newBus = busService.findBusByNumber(busNumber);

        try {
            lineService.updateLine(toChange, newBus);
        } catch (IllegalArgumentException i){
            return HttpStatus.BAD_REQUEST;
        }

        return HttpStatus.OK;

    }
}
