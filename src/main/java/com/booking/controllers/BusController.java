package com.booking.controllers;

import com.booking.domains.Driver;
import com.booking.services.BusService;
import com.booking.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusController {

private BusService busService;

private DriverService driverService;

@Autowired
public void setBusService(BusService busService){this.busService = busService;}
@Autowired
public void setDriverService(DriverService driverService){this.driverService = driverService;}

    @RequestMapping(value = "/buses", method = RequestMethod.POST)
//adds a bus and selects a driver based on last name
public HttpStatus createBus(@RequestParam(value = "model")String model,
                         @RequestParam(value = "number")int number,
                         @RequestParam (value = "driverlastname") String driverLastName){

        Driver toDriveBus = driverService.findByLastName(driverLastName);

        try {
            busService.createBus(model,number,toDriveBus);
        } catch (IllegalArgumentException i){
            return HttpStatus.BAD_REQUEST;
        }

    return HttpStatus.OK;
}


}
