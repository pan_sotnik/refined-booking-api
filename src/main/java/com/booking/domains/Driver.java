package com.booking.domains;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Drivers")
public class Driver {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String lastName;
    private int license;


    protected Driver() {
    }

    public Driver(String name, String lastName, int license) {
        this.name = name;
        this.lastName = lastName;
        this.license = license;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public int getLicenseNumber() {
        return license;
    }

    public void setLicenseNumber(int licenseNumber) {
        this.license = licenseNumber;
    }

}
