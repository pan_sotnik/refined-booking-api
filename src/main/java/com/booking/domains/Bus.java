package com.booking.domains;

import javax.persistence.*;

@Entity
@Table(name="Buses")
public class Bus {

    @Id
    @GeneratedValue
    private int id;

    private String model;
    private int number;

    @OneToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;

    protected Bus() {
    }

    public Bus(String model, int number, Driver driver) {
        this.model = model;
        this.number = number;
        this.driver = driver;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}
