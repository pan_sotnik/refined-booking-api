package com.booking.domains;


import javax.persistence.*;

@Entity
@Table(name="Tickets")
public class Ticket {

    @Id
    @GeneratedValue
    private int id;

    private int seat;
    private int price;
    private boolean paid;

    protected Ticket() {
    }

    public Ticket(int seat, int price, boolean paid) {
        this.seat = seat;
        this.price = price;
        this.paid = paid;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

}
