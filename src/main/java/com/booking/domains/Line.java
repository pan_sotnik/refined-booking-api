package com.booking.domains;


import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Lines")
public class Line {

    @Id
    @GeneratedValue
    private int id;

    private int lineNumber;

    @OneToOne
    @JoinColumn(name = "bus_id")
    private Bus bus;

    @OneToMany
    @JoinTable(name = "Lines_Tickets",
            joinColumns = @JoinColumn(name = "line_id"),
            inverseJoinColumns = @JoinColumn(name = "ticket_id"))
    private Set<Ticket> tickets = new HashSet<Ticket>();

    protected Line() {
    }

    public Line(int lineNumber, Bus bus, Set<Ticket> tickets) {
        this.lineNumber = lineNumber;
        this.bus = bus;
        this.tickets = tickets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }
}

